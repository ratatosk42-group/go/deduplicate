pkgs          = $(shell go list ./... | grep -v /vendor/)

Version   = $(shell cat VERSION)
Revision  = $(shell git log --pretty=format:'%h' -n1)
Branch    = $(shell git symbolic-ref --short HEAD 2> /dev/null)
BuildUser = $(shell whoami)@$(shell hostname)
BuildDate = $(shell date "+%Y.%m.%d.%H%M%S")
BinName   = $(shell basename $$PWD | sed 's/\.git//')

build: staticcheck
	CGO_ENABLED=0 go build -a -tags netgo -ldflags="-s -w -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.BinName=${BinName} -X stdcli.BinName=${BinName} -X git%2ezug%2efr/go/stdcli.BinName=${BinName} -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.Version=${Version} -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.Revision=${Revision} -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.Branch=${Branch} -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.BuildUser=${BuildUser} -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.BuildDate=${BuildDate}" -o $(BinName) ${pkgs}

quickbuild: staticcheck
	go build -ldflags="-s -w -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.BinName=${BinName} -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.Version=${Version}-dev -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.Revision=${Revision} -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.Branch=${Branch} -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.BuildUser=${BuildUser} -X git.zug.fr/go/deduplicate/vendor/git.zug.fr/go/stdcli.BuildDate=${BuildDate}" -o $(BinName) ${pkgs}

updatedeps:
	@test -f glide.yaml && (echo -e "\x1b[33m>> Update dependencies\x1b[0m"; glide update) || echo -e "\x1b[33m>> No dependencies to update\x1b[0m"

all: updatedeps build

install: all
	cp $(BinName) $(GOPATH)/bin/$(BinName)

format:
	@echo -e "\x1b[33m>> Formatting code\x1b[0m"
	@go fmt $(pkgs)

style:
	@echo -e "\x1b[33m>> Checking code style\x1b[0m"
	@! gofmt -d $(shell find . -path ./vendor -prune -o -name '*.go' -print) | grep '^'

staticcheck:
	@echo -e "\x1b[33m>> Running staticcheck\x1b[0m"
	@staticcheck ${pkgs}

test:
	@echo -e "\x1b[33m>> Running tests\x1b[0m"
	@go test -short $(pkgs)

vet:
	@echo -e "\x1b[33m>> Vetting code\x1b[0m"
	@go vet $(pkgs)

.PHONY: all build quickbuild updatedeps staticcheck install format style test vet
