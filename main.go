//    Copyright © 2016  Guillaume Zugmeyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bufio"
	"crypto/sha1"
	"flag"
	"fmt"
	"io"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"git.zug.fr/go/stdcli"
)

const filechunk = 8192

var (
	rootpath = flag.String("path", ".", "The rootpath folder path to check")
	filter   = flag.String("filter", "", "Filter files by extension (comma separated)")
	exclude  = flag.String("exclude", "", "Exclude some path (comma separated)")
	nofollow = flag.Bool("nofollow", false, "Don’t follow symlinks")
	delete   = flag.Bool("delete", false, "Interactive file deletion")

	sums      = make(map[string][]string)
	checked   uint
	found     uint
	foundUniq uint
	filters   []string
	excluded  []string
	err       error
)

func init() {
	flag.StringVar(rootpath, "p", ".", "The rootpath folder path to check")
	flag.StringVar(filter, "f", "", "Filter files by extension (comma separated)")
	flag.StringVar(exclude, "x", "", "Exclude some path (comma separated)")
	flag.BoolVar(nofollow, "nf", false, "Don’t follow symlinks")
	flag.BoolVar(delete, "s", false, "Interactive file deletion")
}

func main() {

	stdcli.Initialize()

	if *filter != "" {
		filters = strings.Split(*filter, ",")
	}
	if *exclude != "" {
		excluded = strings.Split(*exclude, ",")
	}

	err = filepath.Walk(*rootpath, checksumFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Print("\x1b[K")

	if found == 0 {
		fmt.Printf("No duplicated files found (on %d file checked)\n", checked)
	} else {
		fmt.Printf("Found %d duplicated files (on %d file checked)\n\n", found, checked)
	}

	if *delete {
		fmt.Print("\x1b[93mType the index of file you would to KEEP !\x1b[0m\n\n")
	}

	foundIndex := 1
	if found > 0 {
		for _, checkedPaths := range sums {
			if len(checkedPaths) > 1 {

				for index, checkedPath := range checkedPaths {
					fmt.Printf("%d - %s\n", index, checkedPath)
				}

				if *delete {
					scanner := bufio.NewScanner(os.Stdin)
					fmt.Printf("%d / %d > ", foundIndex, foundUniq)
					for scanner.Scan() {
						if scanner.Text() == "" {
							fmt.Print("\x1b[1A\r\x1b[K\n")
							break
						}
						inputIndex, err := strconv.Atoi(scanner.Text())
						if err != nil {
							fmt.Printf("\x1b[1A\r\x1b[K%d / %d > ", foundIndex, foundUniq)
							continue
						}
						if inputIndex > len(checkedPaths)-1 || inputIndex < 0 {
							fmt.Printf("\x1b[1A\r\x1b[K%d / %d > ", foundIndex, foundUniq)
							continue
						}

						for index, checkedPath := range checkedPaths {
							if index != inputIndex {
								fmt.Printf("\x1b[1A\r\x1b[K\x1b[90mRemoving %s\x1b[0m\n\n", checkedPath)
								os.Remove(checkedPath)
							}
						}
						break
					}
				} else {
					fmt.Print("\n")
				}
				foundIndex++
			}
		}
	}
}

func matchFilters(path string, filters []string) bool {
	if len(filters) == 0 {
		return true
	}
	for _, filter := range filters {
		if filepath.Ext(path) == "."+filter {
			return true
		}
	}
	return false
}

func notExcluded(path string, excluded []string) bool {
	if len(excluded) == 0 {
		return true
	}
	for _, fragment := range excluded {
		if strings.HasPrefix(path, fragment) || strings.Contains(path, "/"+fragment) {
			return false
		}
	}
	return true
}

func checksumFile(path string, info os.FileInfo, err error) error {
	if info == nil {
		return nil
	}
	if info.Mode().IsRegular() && matchFilters(path, filters) && notExcluded(path, excluded) {
		file, _ := os.Open(path)
		defer file.Close()

		filesize := info.Size()

		blocks := uint64(math.Ceil(float64(filesize) / float64(filechunk)))

		hash := sha1.New()
		for i := uint64(0); i < blocks; i++ {
			blocksize := int(math.Min(filechunk, float64(filesize-int64(i*filechunk))))

			buf := make([]byte, blocksize)
			file.Read(buf)
			io.WriteString(hash, string(buf))
		}

		fileHash := fmt.Sprintf("%x", hash.Sum(nil))

		checked++

		stdcli.PrintOnCurrentLine(fmt.Sprintf("%d: %s", checked, path))

		if _, ok := sums[fileHash]; !ok {
			sums[fileHash] = append(sums[fileHash], path)
		} else {
			found++
			if len(sums[fileHash]) == 1 {
				foundUniq++
			}
			sums[fileHash] = append(sums[fileHash], path)
		}

	}
	if !*nofollow && info.Mode()&os.ModeSymlink != 0 {
		err := filepath.Walk(path+"/", checksumFile)
		if err != nil {
			fmt.Println(err)
		}
	}
	return nil
}
